mod logic;
mod visual;
mod vendor;
mod input;

fn main() {
    tracing_wasm::set_as_global_default();
    dioxus_web::launch(visual::App);
}
