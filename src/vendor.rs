//! vendor data
//! 
//! default assets that comes with this game editor

use crate::logic::*;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref LEVELS: Vec<Level> = vec![
        // Level::new({
        //     use EntityKind::*;
        //     let mut scene = Scene::default();
        //     scene.add(ivec2(1, 1), Box);
        //     scene.add(ivec2(2, 3), Player);
        //     scene.add(ivec2(5, 5), Air);
        //     scene
        // }),
        Level::from_ascii_art(
            r#"
######
# o. #
#    #
# .o@#
#    #
# oo #
#    #
######
"#).unwrap(),
Level::from_ascii_art(
    r#"
#####
#   #
# @@#
#####
"#).unwrap(),
    ];
}
