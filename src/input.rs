pub struct ActionMap {
    actions: Vec<Action>,
}

#[derive(Debug, thiserror::Error)]
pub enum ValidateActionMapError {
    #[error("Two actions have the same name: {0}")]
    DuplicateName(String),
}

impl ActionMap {
    pub fn validate(&self) -> Result<(), ValidateActionMapError> {
        todo!()
    }
}

pub struct Action {
    name: String,
    keybinds: Vec<String>,
}
