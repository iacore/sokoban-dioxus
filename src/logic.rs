use glam::{ivec2, IVec2};
use std::collections::{BTreeMap, HashSet, VecDeque};
use std::fmt::Write;
use std::sync::Arc;
use strum::IntoEnumIterator;

/// a point in time
pub type Instant = u16;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct TimeMarked<T> {
    pub time: Instant,
    pub data: T,
}

impl<T> TimeMarked<T> {
    pub fn new(time: Instant, data: T) -> Self {
        Self { time, data }
    }
}

pub type TimeGated<T> = VecDeque<TimeMarked<T>>;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EntityKey(pub u16);

#[derive(Clone, PartialEq, Eq, Hash)]
pub struct Entity {
    pub id: EntityKey,
    pub position: TimeGated<IVec2>,
    pub kind: EntityKind,
}

impl Entity {
    pub fn new(id: EntityKey, when: Instant, position: IVec2, kind: EntityKind) -> Self {
        Self {
            id,
            position: [TimeMarked {
                time: when,
                data: position,
            }]
            .into(),
            kind,
        }
    }

    // pub fn spawn_time(&self) -> Instant {
    //     self.position
    //         .front()
    //         .expect("Should have at least one position")
    //         .time
    // }

    pub fn move_absolute(&mut self, when: Instant, position: IVec2) {
        self.position.push_back(TimeMarked::new(when, position));
    }

    pub fn move_relative(&mut self, when: Instant, delta: IVec2) {
        let position = self.current_position() + delta;
        self.position.push_back(TimeMarked::new(when, position));
    }

    pub fn current_position(&self) -> IVec2 {
        self.position
            .back()
            .expect("Should have at least one position")
            .data
    }

    pub fn last_movement(&self) -> Option<TimeMarked<IVec2>> {
        if self.position.len() >= 2 {
            let last = &self.position[self.position.len() - 1];
            let before_last = &self.position[self.position.len() - 2];
            Some(TimeMarked {
                time: last.time,
                data: last.data - before_last.data,
            })
        } else {
            None
        }
    }

    pub fn truncate_history_after(mut self, time: Instant) -> Option<Self> {
        while let Some(x) = self.position.back() {
            if x.time > time {
                self.position.pop_back();
            } else {
                return Some(self);
            }
        }
        return None;
    }

    fn cancel_last_movement(&mut self) {
        self.position.pop_back();
        assert!(self.position.len() >= 1);
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, strum::Display, strum::EnumIter)]
pub enum EntityKind {
    Player,
    Box,
    Wall,
    Target,
}

impl EntityKind {
    pub fn char(&self) -> char {
        use EntityKind::*;
        match self {
            Player => '@',
            Box => 'o',
            Wall => '#',
            Target => '.',
        }
    }
}

#[derive(Debug, thiserror::Error)]
pub enum ApplyRuleError {
    #[error("form a cycle")]
    Loop,
    #[error("can't finish in finite steps")]
    OutOfFuel,
}

#[derive(Default, Clone, PartialEq, Eq, Hash)]
pub struct Scene {
    /// todo: inconsistency: undo will not undo this number
    next_entity_id: u16,
    pub entities: BTreeMap<EntityKey, Entity>,
    pub time: Instant,
}

impl std::fmt::Display for Scene {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for y in 0..10 {
            for x in 0..10 {
                let ents: Vec<_> = self.find(ivec2(x, y)).collect();
                let pad: usize = 3_usize.saturating_sub(ents.len());
                for _ in 0..pad {
                    f.write_char(' ')?;
                }
                for ent in ents {
                    f.write_char(ent.kind.char())?;
                }

                f.write_str(", ")?;
            }
            f.write_str("\n")?;
        }
        Ok(())
    }
}

impl Scene {
    pub fn add(&mut self, position: IVec2, what: EntityKind) -> EntityKey {
        let key = EntityKey(self.next_entity_id);
        self.next_entity_id += 1;
        self.entities
            .insert(key, Entity::new(key, self.time, position, what));
        key
    }

    pub fn find(&self, position: IVec2) -> impl Iterator<Item = &Entity> {
        self.entities
            .values()
            .filter(move |ent| ent.current_position() == position)
    }

    pub fn move_players(&mut self, delta: IVec2) {
        self.time += 1;

        for (_, ent) in self.entities.iter_mut() {
            if ent.kind == EntityKind::Player {
                ent.move_relative(self.time, delta);
            }
        }

        if let Err(e) = self.apply_rules() {
            tracing::debug!(target = "apply_rules", "Failed to apply rules: {e}.");
            match e {
                ApplyRuleError::Loop => {
                    assert!(self.try_undo());
                }
                ApplyRuleError::OutOfFuel => {
                    assert!(self.try_undo());
                }
            }
        } else {
            tracing::debug!(target = "apply_rules", "Succeeded to apply rules.");
        }
    }

    pub fn move_entity(&mut self, id: EntityKey, delta: IVec2) {
        let ent = self
            .entities
            .get_mut(&id)
            .expect("entity should still exist");
        ent.move_absolute(self.time, delta + ent.current_position());
    }

    /// limitation: can't roll back on failure
    /// one rule either wins or fails
    #[tracing::instrument(skip(self))]
    pub fn apply_rules(&mut self) -> Result<(), ApplyRuleError> {
        let mut staging = self.clone();
        let mut seen_states: HashSet<Self> = HashSet::new();
        for _ in 0..37 {
            tracing::debug!(target = "apply_rules", "{state}", state = staging);
            if !seen_states.insert(staging.clone()) {
                return Err(ApplyRuleError::Loop);
            }
            let mut commands = vec![];
            let b = staging.rule_builder();
            for ent in staging.entities.values() {
                use EntityKind::*;
                if b.just_moved(ent) {
                    let last_move = ent.last_movement().expect("just moved").data;
                    if b.on_top_of_kind(ent, Wall).next().is_some() {
                        commands.push(Command::CancelMove(ent.id));
                    }
                    let boxes: Vec<_> = b.on_top_of_kind(ent, Box).collect();
                    if boxes.len() == 1 {
                        commands.push(Command::Move(boxes[0].id, last_move));
                    }
                }
            }
            if commands.is_empty() {
                *self = staging;
                return Ok(());
            }
            for cmd in commands {
                staging.apply_command(cmd);
            }
        }
        Err(ApplyRuleError::OutOfFuel)
    }

    pub fn try_undo(&mut self) -> bool {
        if let Some(time) = self.time.checked_sub(1) {
            self.time = time;
        } else {
            return false;
        }
        self.entities = self
            .entities
            .clone()
            .into_iter()
            .filter_map(|(key, ent)| {
                ent.clone()
                    .truncate_history_after(self.time)
                    .map(|ent2| (key, ent2))
            })
            .collect();
        true
    }

    pub fn apply_command(&mut self, cmd: Command) {
        match cmd {
            Command::Move(id, how_much) => self.move_entity(id, how_much),
            Command::CancelMove(id) => self
                .entities
                .get_mut(&id)
                .expect("entity should still exist")
                .cancel_last_movement(),
        }
    }

    pub fn new() -> Self {
        Self::default()
    }

    pub fn rule_builder<'a>(&'a self) -> rule::RuleBuilder<'a> {
        rule::RuleBuilder::new(self)
    }

    pub fn is_solved(&self) -> bool {
        let b = self.rule_builder();
        for ent in self.entities.values() {
            use EntityKind::*;
            if b.is_kind(ent, Target) && b.on_top_of_kind(ent, Box).next().is_none() {
                return false;
            }
        }
        true
    }
}

#[derive(Clone)]
pub enum Command {
    Move(EntityKey, IVec2),
    CancelMove(EntityKey),
}

#[derive(Clone)]
pub struct Level {
    pub width: i32,
    pub height: i32,
    pub scene: Scene,
    pub ascii_design: Arc<String>,
}

#[derive(Debug, thiserror::Error)]
pub enum MakeLevelError {
    #[error("invalid tile char: {0}")]
    InvalidChar(char),
    #[error("lines are of the same length. inferred: {0}")]
    UnequalLines(usize),
}

impl Level {
    pub fn from_ascii_art(s: &str) -> Result<Self, MakeLevelError> {
        let s = s.trim();
        let mut scene = Scene::new();
        let mut line_length: Option<usize> = None;
        for (y, line) in s.lines().enumerate() {
            if let Some(n) = line_length {
                if line.len() != n {
                    return Err(MakeLevelError::UnequalLines(n));
                }
            } else {
                line_length = Some(line.len());
            }
            for (x, c) in line.chars().enumerate() {
                let tile = match c {
                    ' ' => continue,
                    c => {
                        if let Some(kind) = EntityKind::iter().find(|kind| c == kind.char()) {
                            kind
                        } else {
                            return Err(MakeLevelError::InvalidChar(c));
                        }
                    }
                };
                scene.add(ivec2(x as i32, y as i32), tile);
            }
        }
        Ok(Self {
            width: line_length.unwrap() as i32,
            height: s.lines().count() as i32,
            scene,
            ascii_design: Arc::new(s.to_owned()),
        })
    }
}

pub mod rule {
    use super::*;

    pub struct RuleBuilder<'a> {
        scene: &'a Scene,
    }

    impl<'a> RuleBuilder<'a> {
        pub fn new(scene: &'a Scene) -> Self {
            Self { scene }
        }

        pub fn just_moved(&self, ent: &Entity) -> bool {
            if let Some(last_move) = ent.last_movement() {
                if last_move.time == self.scene.time {
                    return true;
                }
            }
            false
        }

        pub fn on_top_of_kind(
            &self,
            ent: &Entity,
            kind: EntityKind,
        ) -> impl Iterator<Item = &Entity> {
            let pos = ent.current_position();
            let id = ent.id;
            self.scene
                .find(pos)
                .filter(move |other| other.kind == kind && other.id != id)
        }

        pub fn is_kind(&self, ent: &Entity, kind: EntityKind) -> bool {
            ent.kind == kind
        }
    }

    // pub enum RuleEntity {
    //     LastMoved,
    //     IsKind(EntityKind),
    //     Either(Box<RuleEntity>, Box<RuleEntity>),
    //     Both(Box<RuleEntity>, Box<RuleEntity>),
    //     OnTop(Box<RuleEntity>, Box<RuleEntity>),
    // }
}

// pub struct Rule {}

// impl Rule {
//     pub fn apply(&self, scene: &mut Scene) {
//         todo!()
//     }
// }

// pub struct RuleBuilder {}
// impl RuleBuilder {
//     pub fn last_moved(player: EntityKind) -> RuleBuilder {
//         todo!()
//     }
// }

// const rule_move_box: Rule = {
//     use EntityKind::*;
//     todo!("I nede dependent types");
//     RuleBuilder::last_moved(Player).on_top(Box).build(|p, b| vec![Command::MoveEntity(b, p.movement.data)] )
// };
// [ > Player | Box ]
// [ > Box | Box ]
// [ > Player | Player ]
// [ > Player | < Player ]

// type RuleMoveOnTop = Fn() ->

// [ > Player | Box ] -> > Player, > Box

// struct Rule {

// }
