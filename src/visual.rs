#![allow(non_snake_case)] // component names

use std::sync::Arc;

use crate::logic::*;
use crate::vendor::LEVELS;

use dioxus::prelude::*;
use glam::ivec2;
use gloo_events::{EventListener, EventListenerOptions};
use strum::IntoEnumIterator;
use tracing::debug;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{window, KeyboardEvent};

fn handle_key(key: &str, _level: &UseState<usize>, level: &UseRef<Level>) -> bool {
    match key {
        "ArrowLeft" => {
            level.write().scene.move_players(ivec2(-1, 0));
        }
        "ArrowRight" => {
            level.write().scene.move_players(ivec2(1, 0));
        }
        "ArrowDown" => {
            level.write().scene.move_players(ivec2(0, 1));
        }
        "ArrowUp" => {
            level.write().scene.move_players(ivec2(0, -1));
        }
        "z" => {
            level.write().scene.try_undo();
        }
        _ => {
            debug!(target = "handle_key", "unhandled key {:?}", key);
            return false;
        }
    }
    true
}

fn add_onkeydown(level_id: UseState<usize>, level: UseRef<Level>) -> EventListener {
    let document = window().unwrap().document().unwrap();
    let body = document.body().unwrap();
    EventListener::new_with_options(
        &document,
        "keydown",
        EventListenerOptions::run_in_capture_phase(),
        move |evt| {
            let Some(target) = evt.target() else { return };
            if !JsValue::loose_eq(&body, &target) {
                return;
            }
            let evt: &KeyboardEvent = evt.dyn_ref().unwrap();
            if handle_key(&evt.key(), &level_id, &level) {
                evt.prevent_default();
            }
        },
    )
}

fn lines_of_text<'a>(lines: Vec<String>) -> LazyNodes<'a, 'a> {
    let mut nodes = vec![];
    for (i, s) in lines.into_iter().enumerate() {
        if i != 0 {
            nodes.push(rsx! { br {} });
        }
        nodes.push(rsx! { "{s}" });
    }
    rsx! {
        nodes.into_iter()
    }
}

#[inline_props]
pub fn StackedImages(cx: Scope, items: Vec<EntityKind>) -> Element {
    render! {
        items.iter().map(|kind| {
            rsx! {
                div {
                    class: "entity-image entity-{kind} absolute h-16 w-16"
                }
            }
        })
    }
}

#[inline_props]
pub fn GameGrid<'a>(
    cx: Scope,
    level: &'a UseRef<Level>,
    use_sprite: &'a UseState<bool>,
) -> Element {
    render! {
        div {
            class: "p-2",
            (0..level.read().height).map(|y| rsx! {
                div {
                    class: "flex",
                    (0..level.read().width).map(|x|
                        {
                            let level = level.read();
                            let it = level.scene.find(ivec2(x, y));
                            if *use_sprite.get() {
                                let ent_kinds_stacked: Vec<_> = it.map(|e| e.kind).collect();
                                rsx! {
                                    div {
                                        class: "h-16 w-16 -m-[0.5px] border border-black",
                                        StackedImages {
                                            items: ent_kinds_stacked
                                        }
                                    }
                                }
                            } else {
                                let text = lines_of_text(it.map(|e| format!("{}{}", e.kind, e.id.0)).collect());
                                rsx! {
                                    div {
                                        class: "h-16 w-16 -m-[0.5px] border border-black flex items-center justify-center text-center",
                                        text
                                    }
                                }
                            }
                        }
                    )
                }
            })
        }
    }
}

pub fn Instructions(cx: Scope) -> Element {
    render! {
        div {
            h2 { "How to play" }
            p {
                "Movement: " kbd { "↑" } kbd { "↓" } kbd { "←" } kbd { "→" }
                br {}
                "Undo: " kbd { "Z" }
                br {}
                "Restart level: Click level again. You can't undo restart."
            }
            p { "For now, you can only push the box around." }
            h2 { "Frequently Unasked Questions" }
            details {
                summary { "What?" }
                a { href:"https://git.envs.net/iacore/sokoban-dioxus", "Source Code" }
                details {
                    summary { "Cargo.toml" }
                    pre { include_str!("../Cargo.toml") }
                }
            }
            details {
                summary { "Why?" }
                p { "Type safety. Turns out I need dependent types after all. :(" }
            }
        }
    }
}

#[inline_props]
pub fn Controls<'a>(
    cx: Scope,
    original_rule: &'a UseRef<Arc<String>>,
    current_level: &'a UseRef<Level>,
) -> Element {
    let error_message: &UseState<Option<String>> = use_state(cx, || None);

    let make_level = move |s: &str| match Level::from_ascii_art(s) {
        Ok(new_level) => {
            error_message.set(None);
            current_level.set(new_level);
        }
        Err(err) => {
            error_message.set(Some(err.to_string()));
        }
    };
    let error_message = error_message.clone();

    let level_design_ascii = use_state(&cx, || String::from(""));
    use_effect(cx, original_rule.clone(), |original_rule| {
        level_design_ascii.set(original_rule.read().as_ref().clone());
        async {}
    });

    render! {
        div {
            class: "flex flex-col",
            h2 { "Level Editor" }
            div {
                class: "flex gap-2",
                EntityKind::iter().map(|kind| {
                    rsx! {
                        div {
                            "{kind}"
                            span { class: "entity-kind", "{kind.char()}" }
                        }
                    }
                })
            }
            div {
                "\u{feff}"//zero-width nbsp
                span {
                    class: "bg-orange-300",
                    error_message.as_ref().map(|x| { rsx!{ "{x}" } })
                }
            }
            textarea {
                class: "flex-grow font-mono border border-black",
                style: "letter-spacing: 1ch;",
                oninput: move |evt| {
                    tracing::info!("{:?}", evt.data.value);
                    level_design_ascii.set(evt.data.value.clone());
                    make_level(&evt.data.value);
                },
                value: "{level_design_ascii}",
            }
        }
    }
}

fn level_select_button_class(is_selected: bool) -> String {
    format!(
        "m-2 rounded border border-orange-500 p-2 hover:bg-orange-100{}",
        if is_selected { " bg-orange-100" } else { "" }
    )
}

fn tick_or_cross(t: bool) -> &'static str {
    if t {
        "✅"
    } else {
        "❎"
    }
}

#[inline_props]
pub fn LevelSelect<'a>(
    cx: Scope,
    level_id: &'a UseState<usize>,
    level_solved: &'a UseRef<Vec<bool>>,
) -> Element {
    render! {
        div {
            class: "flex flex-wrap",
            (0..LEVELS.len()).map(|i|
                rsx! {
                    button {
                        class: "{level_select_button_class(**level_id == i)}",
                        onclick: move |_| level_id.set(i),
                        "{i}"
                        tick_or_cross(level_solved.read()[i])
                    }
                })
        }
    }
}

#[inline_props]
pub fn LevelStatus<'a>(
    cx: Scope,
    level: &'a UseRef<Level>,
    use_sprite: &'a UseState<bool>,
) -> Element {
    render! {
        div {
            "Solved: {tick_or_cross(level.read().scene.is_solved())} "
            "Steps: {level.read().scene.time} "
            "Use Sprite:"
            input { "type": "checkbox", checked: "{use_sprite}", onchange: |evt| use_sprite.set(evt.data.value.parse().expect("should be true/false")) }
            " "
        }
    }
}

pub fn GameTab(cx: Scope) -> Element {
    let use_sprite = use_state(cx, || true);
    let level_id = use_state(cx, || 0);
    let current_level = use_ref(cx, || LEVELS[0].clone());
    // todo: persist this
    let level_solved = use_ref(cx, || LEVELS.iter().map(|_| false).collect::<Vec<_>>());
    let original_rule = use_ref(cx, || Arc::new("uninit".to_owned()));

    // use_coroutine(cx, init)
    use_effect(cx, level_id, |level_id| {
        let level = &LEVELS[*level_id];
        original_rule.set(level.ascii_design.clone());
        current_level.set(level.clone());
        async {}
    });

    use_effect(cx, current_level, |level| {
        if level.read().scene.is_solved() {
            level_solved.write()[**level_id] = true;
        }
        async move {}
    });

    cx.use_hook(|| add_onkeydown(level_id.clone(), current_level.clone()));

    render! {
        h1 { "Sokoban + Dioxus POC" br {} span { class: "screen-too-small bg-yellow-300 px-1", "Your screen is too small." } }
        div {
            class: "flex",
            div {
                LevelSelect { level_id: level_id, level_solved: level_solved }
                LevelStatus {
                    level: current_level,
                    use_sprite: use_sprite,
                }
                GameGrid { level: current_level, use_sprite: use_sprite }
            }
            Controls {
                original_rule: original_rule,
                current_level: current_level,
            }
        }
        Instructions {}
    }
}

pub fn ActionBinder(cx: Scope) -> Element {
    render! {
        div {}
    }
}

pub fn App(cx: Scope) -> Element {
    #[derive(Clone, Copy, PartialEq, Eq, strum::Display, strum::EnumIter)]
    enum Tabs {
        Game,
        ActionBinder,
    }

    let selected_tab = use_state(cx, || Tabs::Game);

    render! {
        body {
            class: "p-2",
            div {
                Tabs::iter().map(|tab| rsx! {
                    button {
                        class: "{level_select_button_class(*selected_tab == tab)}",
                        onclick: move |_| { selected_tab.set(tab) },
                        "{tab}"
                    }
                })
            }

            match selected_tab.get() {
                Tabs::Game => rsx! { GameTab {} },
                Tabs::ActionBinder => rsx! { ActionBinder {} },
            }
        }
    }
}
