.PHONY: all css build publish dev

#DIST_DIR=dist
DIST_DIR=${TRUNK_STAGING_DIR}

all: ${DIST_DIR}/index.css ${DIST_DIR}/atlas.webp

${DIST_DIR}/index.css: index.css tailwind.config.js index.html src/**.rs
	pnpm tailwindcss -i $< -o $@

${DIST_DIR}/atlas.webp: atlas.aseprite
	aseprite -b $< --ignore-layer Background --save-as $@

build:
	trunk build --release

publish: build
	surge dist sokoban-dioxus.surge.sh

dev:
	trunk serve

